#include <stdio.h>
#include <stdint.h>
#include <chibi/sexp.h> // Interface with Chibi Scheme
#include <SDL2/SDL.h> // SDL2 standard functions
#include <SDL2/SDL2_gfxPrimitives.h> // SDL2 shapes and other handy abstractions.

// Include SDL's audio mixing components
#include <SDL2/SDL_mixer.h>

// Include a .h file containing the math for generating a frequency.
#include "tone.h"



SDL_Window* sdlWindow;
SDL_Renderer* sdlRenderer;
SDL_Texture* sdlTexture;
uint32_t* pixels;
int running = 1;
uint32_t SCREEN_W = 1920/2;
uint32_t SCREEN_H = 1080/2;

// music, a pointer for all our SDL audio objects. My original idea
// was to set this pointer to NULL every time we need to load an audio
// file, but it may be cleaner to have a (make-audio name1 "file.wav")
// function from Scheme, which is callable with (play-audio name1) and
// (ause-audio name1).
Mix_Music *music = NULL;

typedef struct scheme_input {
  uint32_t mouse_buttons;
  int32_t mouse_x;
  int32_t mouse_y;
  int32_t keycode;
} scheme_input_t;

static scheme_input_t scheme_input;

void sdl_pixel_put(int x, int y, uint32_t color)
{
  *(pixels + y*SCREEN_W + x) = color;
}

static sexp pixel_put(sexp ctx, sexp self, sexp n, sexp sx_x, sexp sx_y, sexp sx_color) {
  uint32_t color = (uint32_t)sexp_unbox_fixnum(sx_color);
  sdl_pixel_put(sexp_unbox_fixnum(sx_x),sexp_unbox_fixnum(sx_y),(color));
  return n;
}

// Scheme function to set the window width/height
static sexp set_window_dimensions(sexp ctx, sexp self, sexp n, sexp sx_height, sexp sx_width) {
  uint32_t height = (uint32_t)sexp_uint_value(sx_height);
  uint32_t width = (uint32_t)sexp_uint_value(sx_width);
  SCREEN_H = height;
  SCREEN_W = width;
  return SEXP_TRUE;
}

static sexp pixel_rect_fill(sexp ctx, sexp self, sexp n, sexp sx_x, sexp sx_y, sexp sx_x2, sexp sx_y2, sexp sx_color) {
  uint32_t color = (uint32_t)sexp_uint_value(sx_color);
  uint32_t x = (uint32_t)sexp_uint_value(sx_x);
  uint32_t y = (uint32_t)sexp_uint_value(sx_y);
  uint32_t w = (uint32_t)sexp_uint_value(sx_x2)-x;
  uint32_t h = (uint32_t)sexp_uint_value(sx_y2)-y;

  for (int iy=0; iy<h; iy++) {
    for (int ix=0; ix<w; ix++) {
      sdl_pixel_put(x+ix,y+iy,color);
    }
  }
  return n;
}

// pixel_ellipse_fill, which turns SDL2_gfxPrimitives'
// filledEllipseColor into a Scheme function. This currently does not
// work properly. You can call it but the most that it will be able to
// display is a single pixel. Will probably be fixed soon.
static sexp pixel_ellipse_fill(sexp ctx, sexp self, sexp n, sexp sx_x, sexp sx_y, sexp sx_w, sexp sx_h, sexp sx_color) {
  uint32_t color = (uint32_t)sexp_uint_value(sx_color);
  uint32_t x =     (uint32_t)sexp_uint_value(sx_x);
  uint32_t y =     (uint32_t)sexp_uint_value(sx_y);
  uint32_t w =     (uint32_t)sexp_uint_value(sx_w);
  uint32_t h =     (uint32_t)sexp_uint_value(sx_h);
  uint8_t coloro[4];
  coloro[3] = (color & 0x000000ff);
  coloro[2] = (color & 0x0000ff00) >> 8;
  coloro[1] = (color & 0x00ff0000) >> 16;
  coloro[0] = (color & 0xff000000) >> 24;
  filledEllipseRGBA(sdlRenderer, x, y, w, h, coloro[0], coloro[1], coloro[2], coloro[3]);
  return n;
  }

// stroke_line, a simple function for drawing a line.
// color is a uint32_t, like 0xFF00FFFF, but SDL_SetRenderDrawColor
// takes 4 Uint8's as parameters, so we want to split our uint32 into
// 4 uint8's, and use the 4 fractions as arguments for the color.
// Note that you MUST also specify the alpha by using 8-bits
// (like 0xFFFFFFFF instead of 0xFFFFFF or 0xFFF).
void sdl_stroke_line(int x1, int y1, int x2, int y2, uint32_t color) {
  uint8_t coloro[4]; // array of colors, a Uint32_t split into 4-parts.
  coloro[3] = (color & 0x000000ff);
  coloro[2] = (color & 0x0000ff00) >> 8;
  coloro[1] = (color & 0x00ff0000) >> 16;
  coloro[0] = (color & 0xff000000) >> 24;
  SDL_SetRenderDrawColor(sdlRenderer, coloro[0], coloro[1], coloro[2], coloro[3]);
  SDL_RenderDrawLine(sdlRenderer, x1, y1, x2, y2); // Draw the line.
}

// This is the s-expression function to call sdl_stroke_line.
// Maybe we should make a sexp->uint32_t convert function
// for readability in the future.
sexp stroke_line(sexp ctx, sexp self, sexp n, sexp sx_x1,
		 sexp sx_y1, sexp sx_x2, sexp sx_y2, sexp sx_color) {
  uint32_t color = (uint32_t)sexp_uint_value(sx_color);
  uint32_t x1 = sexp_unbox_fixnum(sx_x1);
  uint32_t y1 = sexp_unbox_fixnum(sx_y1);
  uint32_t x2 = sexp_unbox_fixnum(sx_x2);
  uint32_t y2 = sexp_unbox_fixnum(sx_y2);
  sdl_stroke_line(x1, y1, x2, y2, (color));
  // returning n yields a segmentation fault.
  // using sx_x1 just because it works.
  return sx_x1;
}

sexp get_mouse_x(sexp ctx, sexp self, sexp n) {
  return sexp_make_integer(ctx, scheme_input.mouse_x);
}

sexp get_mouse_y(sexp ctx, sexp self, sexp n) {
  return sexp_make_integer(ctx, scheme_input.mouse_y);
}

sexp get_mouse_buttons(sexp ctx, sexp self, sexp n) {
  return sexp_make_integer(ctx, scheme_input.mouse_buttons);
}

sexp get_key(sexp ctx, sexp self, sexp n) {
  return sexp_make_integer(ctx, scheme_input.keycode);
}

////////////////////// BEGIN MAIN AUDIO CODE

// generate_tone, allows us to make a beep sound from Scheme, with:
// (generate-tone 440.0 0.5) ;;440Hz is the key of A, 0.5 seconds.
// Requires much more testing.
static sexp generate_tone(sexp ctx, sexp self, sexp n,
			  sexp sx_tone, sexp sx_duration) {
  float tone = (float)sexp_flonum_value(sx_tone);
  float duration = (float)sexp_flonum_value(sx_duration);
  sdl_generate_tone(tone, duration);
  return SEXP_TRUE;
}

// Load a WAV file. 'filename' is the wav file,
// loopp is a number specifying whether the audio
// should loop forever.
int sdl_play_wav_file(char* filename, int loopp) {
  //INIT AUDIO
  if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
    printf("Can't start SDL_mixer! %s\n", Mix_GetError());
    return 1;
  }
  music = Mix_LoadMUS(filename);
  Mix_PlayMusic(music, loopp);
  if (music == NULL) {
    printf("Can't start music! %s\n", Mix_GetError());
    return 1;
  }

  return 0;
}

// The S-Expression version of sdl_play_wav_file,
// that we provide to Scheme.
sexp play_wav_file(sexp ctx, sexp self, sexp n,
		      sexp filename, sexp sx_loopp) {
  uint32_t loopp = sexp_unbox_fixnum(sx_loopp);
  char* filen = sexp_string_data(filename);

  sdl_play_wav_file(filen, loopp);
  return n;
}
////////////////////// END OF AUDIO SECTION


// 'provide', a shortcut for sexp_define_foreign.
void provide(sexp ctx, sexp env, char* name, int num_args, int return_type, void* func) {
  sexp op = sexp_define_foreign(ctx, env, name, num_args, func);
  if (sexp_opcodep(op)) {
    if (return_type == 0) {
      sexp_opcode_return_type(op) = SEXP_VOID;
    } else {
      sexp_opcode_return_type(op) = SEXP_OBJECT;
    }
  } else {
    printf("could not register %s!\n", name);
  }
}

// This is where we send C functions to be used in Scheme.
// TODO: 'provide' might be too generic of a name
//       (and thus, may conflict in the future).
void scheme_define_ops(sexp ctx, sexp env) {
  provide(ctx, env, "pixel-put", 3, 0, (sexp_proc4)pixel_put);
  provide(ctx, env, "stroke-line", 5, 0, (sexp_proc6)stroke_line);
  provide(ctx, env, "pixel-rect-fill", 5, 0, (sexp_proc6)pixel_rect_fill);
  provide(ctx, env, "pixel-ellipse-fill", 5, 0, (sexp_proc6)pixel_ellipse_fill);
  provide(ctx, env, "set-window-dimensions!", 2, 0, (sexp_proc1)set_window_dimensions);
  provide(ctx, env, "mouse-x", 0, 1, (sexp_proc1)get_mouse_x);
  provide(ctx, env, "mouse-y", 0, 1, (sexp_proc1)get_mouse_y);
  provide(ctx, env, "mouse-buttons", 0, 1, (sexp_proc1)get_mouse_buttons);
  provide(ctx, env, "keyboard", 0, 1, (sexp_proc1)get_key);
  provide(ctx, env, "play-wav-file", 2, 0, (sexp_proc1)play_wav_file);
  provide(ctx, env, "generate-tone", 2, 0, (sexp_proc1)generate_tone);
}

void scheme_loop(sexp ctx) {
  // TODO: record elapsed time and input events and pass to main function for animation stuff?
  SDL_UpdateTexture(sdlTexture, NULL, pixels, SCREEN_W * sizeof(uint32_t));
  SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, NULL);
  // evaluate the (main) function and print any errors
  sexp res;
  res = sexp_eval_string(ctx, "(main)", -1, NULL);
  if (sexp_exceptionp(res)) {
    sexp_print_exception(ctx, res, sexp_current_error_port(ctx));
  }
  SDL_RenderPresent(sdlRenderer);
}

int main(int argc, char** argv) {
  sexp ctx;
  SDL_Event event;

    sexp_scheme_init();
  ctx = sexp_make_eval_context(NULL, NULL, NULL, 1024*1024*64, 1024*1024*1024);
  sexp res;
  res = sexp_load_standard_env(ctx, NULL, SEXP_SEVEN);
  if (sexp_exceptionp (res)) {
    sexp_print_exception(ctx, res, sexp_current_error_port(ctx));
    exit(1);
  }
  sexp_load_standard_ports(ctx, NULL, stdin, stdout, stderr, 1);
  scheme_define_ops(ctx, sexp_context_env(ctx));
  sexp_gc_var1(obj1);
  sexp_gc_preserve1(ctx, obj1);
  if (argc == 2) { // If the user specified a file
  obj1 = sexp_c_string(ctx, argv[1], -1);
  } else if (argc > 2) {
    printf("Too many arguments!\n");
  } else { // If 0 arguments, use the default file.
    obj1 = sexp_c_string(ctx, "./init.scm", -1);
  }

  SDL_UpdateTexture(sdlTexture, NULL, pixels, SCREEN_W * sizeof(uint32_t));
  SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, NULL);

  /*If the Scheme file has errors in it, we want to print those errors
    to the console. We put each Scheme operation in a 'result' variable
    (res), which is passed to sexp_exceptionp() to check for errors. */
  res = sexp_load(ctx, obj1, NULL); // Load obj1, the Scheme file
  if (sexp_exceptionp (res)) { // If there was an exception (error)
    // Print the error to the current error port.
    sexp_print_exception(ctx, res, sexp_current_error_port(ctx));
    exit(1);
  }

  // Initialize the SDL video/audio components.
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
    printf("Couldn't initialize SDL! %s\n", SDL_GetError());
    return 1;
  }

  // Uncommenting the following two lines turns on pixel smoothing.
  // SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
  // SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,  1);
  
  // The SDL window defines the window title, position, and size,
  // along with how to render it.
  sdlWindow = SDL_CreateWindow("SomeTitle",
			       SDL_WINDOWPOS_UNDEFINED,
			       SDL_WINDOWPOS_UNDEFINED,
			       SCREEN_W, SCREEN_H,
			       SDL_WINDOW_OPENGL);
  if (!sdlWindow) {
    printf("Can't open window! %s\n", SDL_GetError());
    return 1;
  }
      
  // The SDL renderer describes what window to draw on, -1
  // automatically picks the rendering driver to initialize, and
  // "SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC" specify to
  // match the FPS to the monitor framerate.  Note that
  // SDL_RENDERER_PRESENTVSYNC is not present. This flag slows down
  // the rendering too much and needs further testing.
  sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_ACCELERATED);
  if (!sdlRenderer) {
    printf("Can't start renderer! %s\n", SDL_GetError());
    return 1;
  }

  // Create a texture as big as the screen.
  // The texture is the actual pixel data.
  sdlTexture = SDL_CreateTexture(sdlRenderer,
			       SDL_PIXELFORMAT_ARGB8888,
			       SDL_TEXTUREACCESS_STREAMING,
			       SCREEN_W, SCREEN_H);

  SDL_RenderSetLogicalSize(sdlRenderer, SCREEN_W, SCREEN_H);

  pixels = (uint32_t*)malloc(SCREEN_W*SCREEN_H*sizeof(uint32_t));

  // The game loop starts here.
  while (running) {
    if (SDL_PollEvent(&event)) {
      switch (event.type) {
      case SDL_QUIT:
	running = 0;
	break;
      case SDL_KEYDOWN:
	{
	  int c = event.key.keysym.sym;
	  // FIXME this is obvs. very primitive (only 1 key allowed at a time)
	  scheme_input.keycode = c;
	  if (c==27) {
	    // Escape
	    running = 0;
	  }
	  break;
	}
      case SDL_KEYUP:
	{
	  int c = event.key.keysym.sym;
	  scheme_input.keycode = 0;
	  break;
	}
      case SDL_MOUSEMOTION:
	{
	  scheme_input.mouse_x = event.motion.x;
	  scheme_input.mouse_y = event.motion.y;
	  break;
	}
      case SDL_MOUSEBUTTONDOWN:
	{
	  scheme_input.mouse_buttons |= (1<<(event.button.button-1));
	  break;
	}
      case SDL_MOUSEBUTTONUP:
	{
	  scheme_input.mouse_buttons &= ~(1<<(event.button.button-1));
	  break;
	}
      }
    }

    scheme_loop(ctx);
  }

  sexp_destroy_context(ctx);
  SDL_Quit();
  free(pixels);
}
