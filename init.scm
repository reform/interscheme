;;; defined in C code
;;; (pixel-put x y color)
;;; (pixel-rect-fill x1 y1 x2 y2 color)
;;; (stroke-line x1 y1 x2 y2 color)

(import (srfi 27))

(define (pixel-hline x1 x2 y color)
  (if (> x1 x2)
      #t
      (begin
	(pixel-put x1 y color)
	(pixel-hline (+ x1 1) x2 y color))))

(define (pixel-vline x y1 y2 color)
  (if (> y1 y2)
      #t
      (begin
	(pixel-put x y1 color)
	(pixel-vline x (+ y1 1) y2 color))))

(define (pixel-rect x1 y1 x2 y2 color)
  (pixel-hline x1 x2 y1 color)
  (pixel-hline x1 x2 y2 color)
  (pixel-vline x1 y1 y2 color)
  (pixel-vline x2 y1 y2 color))

(display "hello from scheme world!\n")

(define this-key 0)
(define last-key 0)
(define paint-color #xffffff)
(define random-color-mode #t)

;; Play a wav file when the program starts.
(play-wav-file "Test.wav"  0)

;; Play the wav file AND loop forever:
;; (play-wav-file "Test.wav" -1)

(define (main)
  ;;; (display "(main)\n")
  (pixel-rect 32 32 128 128 #xffffff)
  (pixel-rect-fill 150 32 250 128 #xffff00)
  (stroke-line 300 32 350 128 #x00ffffff)
  (pixel-ellipse-fill 420 75 20 60 #xffaaaaff)

  (if (= (mouse-buttons) 1)
      (pixel-rect-fill
       (mouse-x) (mouse-y)
       (+ (mouse-x) 8) (+ (mouse-y) 8)
       paint-color))

  (if (= (mouse-buttons) 4)
      (pixel-rect-fill
       (mouse-x) (mouse-y)
       (+ (mouse-x) 32) (+ (mouse-y) 32)
       #x000000))

  (set! this-key (keyboard))

  (if random-color-mode
    (set! paint-color (random-integer #xffffff)))

  (if (> this-key 0)
      (if (not (= this-key last-key))
          (begin
            (display (list "key: " this-key))
            (display "\n")
            (cond
             ((= this-key 49) (set! random-color-mode #t))
             ((= this-key 50) (set! random-color-mode #f))
             ((= this-key 51) (set! paint-color #x0000ff))
             ((= this-key 52) (set! paint-color #xff00ff))
             ((= this-key 53) (set! paint-color #x00ffff))
             ((= this-key 54) (set! paint-color #xffff00))
             ((= this-key 55) (set! paint-color #xffffff))
            ))))

  (set! last-key (keyboard))
)
