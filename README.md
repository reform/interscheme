# Interscheme

A pixel graphics and sound playground using SDL2 and chibi-scheme.

WIP.

See also: http://synthcode.com/scheme/chibi/#h3_QuickStart

## Build

Requirements:

- Chibi Scheme
- SDL2
- SDL2 Mixer
- SDL2 GFX

First, build and install chibi-scheme:

```bash
git clone https://github.com/ashinn/chibi-scheme.git
cd chibi-scheme
make -j$(nproc)
sudo make install
```

This will yield `libchibi-scheme.so` in `/usr/local/lib` and `/usr/local/include/chibi` header files. You might need to do `ldconfig /usr/local/lib` once.

It will also make a scheme REPL called `chibi-scheme`.

Then, in this here repo:

```bash
./build.sh
```

## Run

Edit `init.scm` to your liking.

```bash
./interscheme
```

## Documentation

Interscheme comes with several Scheme functions that are high-level 
wrappers to SDL functions. This is a complete list of 
readily-available functions (tagged with "no" or "yes" depending on 
whether or not the functionality is complete & working or not)

- Graphical Functions
    - `(pixel-put x y #xffffff) ; yes`
    - `(stroke-line x1 y1 x2 y2 #xffffffff) ; yes`
    - `(pixel-rect-fill x1 y1 x2 y2 #xffffff) ; yes`
    - `(pixel-ellipse-fill x1 y1 x2 y2 #xffffff) ; no`
    - `(get-pixel-value x y) ; no`
    - `(set-fps n) ; no`
- Audio Functions
    - `(play-wav-file "some_file.wav") ; yes`
    - `(generate-tone frequency duration) ; wip`
- Input Functions
    - `(mouse-x) ; yes`
    - `(mouse-y) ; yes`
    - `(keyboard) ; yes`
    - `(mouse-buttons) ; yes`
    - `(joystick) ; no`

Interscheme is documented in more detail in the Reform Handbook.