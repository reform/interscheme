;;; An example file to demonstrate/test the
;;; audio functionality of Interscheme.

;; Play a wav file when the program starts.
(play-wav-file "Test.wav"  0)

;; Play the wav file AND loop forever:
;;(play-wav-file "Test.wav" -1)

;; We define a single bool that acts as a switch.
;; Once the audio plays once, turn the switch off.
;; This lets the program know we don't want to loop forever.
(define play-sound1? #t)

;; Define a function that plays 3 tones
(define (sound1)
  (display "Playing sound1\n")
  (generate-tone 440.0 0.4)
  (generate-tone 700.0 0.5)
  (generate-tone 440.0 0.4)
  (set! play-sound1? #f))

(define (main)
;;  (display "main\n")
;;  (pixel-rect 32 32 128 128 #xffffff)
  ;; 
  (if play-sound1?
   (sound1)))
